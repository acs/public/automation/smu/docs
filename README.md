# **SMU** <br/> _Synchronized Measurement Unit_

The ramping trend of cheap and performant single board computers (SBC) is growingly offering unprecedented opportunities in various domains, taking advantage of the widespread support and flexibility offered by an operating system (OS) environment. Unfortunately, data acquisition systems implemented in an OS environment are traditionally considered not to be suitable for reliable industrial applications. Such a position is supported by the lack of hardware interrupt handling and deterministic control of timed operations. In this work the authors fill this gap by proposing an innovative and versatile SBC-based open-source platform for CPU-independent data acquisition. The synchronized measurement unit (SMU) is a high-accuracy device able to perform multichannel simultaneous sampling up to 200 kS/s with sub-microsecond synchronization precision to a GPS time reference. It exhibits very low offset and gain errors, with a minimum bandwidth beyond 20 kHz, SNR levels above 90 dB and THD as low as −110 dB. These features make the SMU particularly attractive for the power system domain, where synchronized measurements are increasingly required for the geographically distributed monitoring of grid operating conditions and power quality phenomena. We present the characterization of the SMU in terms of measurement and time synchronization accuracy, proving that this device, while low-cost, guarantees performance compliant with the requirements for synchrophasor-based applications in power systems.

[<img src="docs/smu_architecture.png"  width="800" height="461">](https://www.mdpi.com/1424-8220/22/14/5074)

_Click on the image to go to the document page_

## Copyright

2017-2022, Carlo Guarnieri (ACS) <br/>
2019-2023, Cesar Cazal (ACS) <br/>
2021, Institute for Automation of Complex Power Systems, EONERC

## License

<a rel="license" href="http://creativecommons.org/licenses/by/4.0/"><img alt="Creative Commons License" style="border-width:0" src="https://i.creativecommons.org/l/by/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">Creative Commons Attribution 4.0 International License</a>.

## Funding
<a rel="funding" href="https://hyperride.eu/"><img alt="HYPERRIDE" style="border-width:0" src="docs/hyperride_logo.png" height="63"/></a>&nbsp;
<img alt="PLATONE" style="border-width:0" src="docs/platone_logo.png" height="63"/></a>&nbsp; 
<a rel="funding" href="https://cordis.europa.eu/project/id/957788"><img alt="H2020" style="border-width:0" src="https://hyperride.eu/wp-content/uploads/2020/10/europa_flag_low.jpg" height="63"/></a><br />
This work was supported by <a rel="Hyperride" href="https://hyperride.eu/">HYbrid Provision of Energy based on Reliability and Resiliency via Integration of DC Equipment</a> (HYPERRIDE) and <a rel="Platone" href="https://platone-h2020.eu/"> PLATform for Operation of distribution NEtworks (Platone), projects funded by the European Union's Horizon 2020 research and innovation programme under <a rel="H2020" href="https://cordis.europa.eu/project/id/957788"> grant agreement No. 957788</a> and <a rel="H2020" href="https://cordis.europa.eu/project/id/864300"> grant agreement No. 864300, respectively.

## Contact

[![EONERC ACS Logo](docs/eonerc_logo.png)](http://www.acs.eonerc.rwth-aachen.de)

- [Cesar Andres Cazal, M.Sc.](mailto:cesar.cazal@eonerc.rwth-aachen.de)

[Institute for Automation of Complex Power Systems (ACS)](http://www.acs.eonerc.rwth-aachen.de)  
[E.ON Energy Research Center (EONERC)](http://www.eonerc.rwth-aachen.de)  
[RWTH University Aachen, Germany](http://www.rwth-aachen.de)
